/* hotinhere, minimalistic desktop temperature monitor.
 * Copyright (C) 2017 Julien JPK.

 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.

 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
 * details.
 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#include <sensors/sensors.h>
#include <libnotify/notify.h>

#define ANY_SENSOR 0
#define ALL_SENSORS 1
#define MEAN_VALUE 2

#define FALLBACK_LOW 80
#define FALLBACK_CRIT 100

#define DEFAULT_TAKEIN 2
#define DEFAULT_INTER 5
#define DEFAULT_WAIT 300

unsigned char running = 1;

void sighandler(int signum);
void usage(char* prog);

int main(int argc, char* argv[]) {
    const sensors_chip_name* chip;
    const sensors_feature* feat;
    const sensors_subfeature* subfeat;
    double low = 0xFF, critical = 0xFF, value, sum;
    long takein = DEFAULT_TAKEIN, inter = DEFAULT_INTER, wait = DEFAULT_WAIT;
    char trigger = ANY_SENSOR, *icon;
    int n = 0, tlow = 0, tcrit = 0, nlow, ncrit;

    NotifyNotification* notif;
    NotifyUrgency urgency;
    struct sigaction sa = {0};

    /* Handle base signals. */
    sa.sa_handler = sighandler;
    sigaddset(&(sa.sa_mask), SIGINT);
    sigaddset(&(sa.sa_mask), SIGTERM);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
    
    /* Initialise our two libraries. */
    if(sensors_init(NULL) != 0) {
        fprintf(stderr, "%s: can't access sensor data.\n", argv[0]);
        return EXIT_FAILURE;
    }

    if(!notify_init(argv[0])) {
        fprintf(stderr, "%s: can't use the notifications library.", argv[0]);
        return EXIT_FAILURE;
    }

    /* Get default values from sensors. */
    int i = 0;
    while((chip = sensors_get_detected_chips(NULL, &i))) {
        int j = 0;
        while((feat = sensors_get_features(chip, &j))) {
            int k = 0;
            while((subfeat = sensors_get_all_subfeatures(chip, feat, &k))) {
                sensors_get_value(chip, subfeat->number, &value);
                switch(subfeat->type) {
                    case SENSORS_SUBFEATURE_TEMP_MAX:
                        if(value < low) low = value;
                        break;
                    case SENSORS_SUBFEATURE_TEMP_CRIT:
                    case SENSORS_SUBFEATURE_TEMP_LCRIT:
                    case SENSORS_SUBFEATURE_TEMP_EMERGENCY:
                        if(value < critical) critical = value;
                        break;
                }
            }
        }
    }

    /* Parse options. */
    char c;
    while((c = getopt(argc, argv, "hoaml:c:i:n:w:")) > 0) {
        switch(c) {
            case 'h':
                usage(argv[0]);
                return EXIT_SUCCESS;

            case 'o':
                trigger = ANY_SENSOR;
                break;
            case 'a':
                trigger = ALL_SENSORS;
                break;
            case 'm':
                trigger = MEAN_VALUE;
                break;

            case 'l':
                low = (int)strtol(optarg, NULL, 10);
                break;
            case 'c':
                critical = (int)strtol(optarg, NULL, 10);
                break;

            case 'i':
                inter = strtol(optarg, NULL, 10);
                break;
            case 'n':
                takein = strtol(optarg, NULL, 10);
                break;
            case 'w':
                wait = strtol(optarg, NULL, 10);
                break;
        }
    }

    if(low >= 0xFF) low = FALLBACK_LOW;
    if(critical >= 0xFF) critical = FALLBACK_CRIT;
    if(low > critical) {
        low += critical;
        critical = low - critical;
        low -= critical;
    }
    
    /* Running forever. */
    while(running) {
        n = nlow = ncrit = sum = 0;

        /* Scan all sensors, find problematic values and compute the average. */
        int i = 0;
        while((chip = sensors_get_detected_chips(NULL, &i))) {
            int j = 0;
            while((feat = sensors_get_features(chip, &j))) {
                int k = 0;
                while((subfeat = sensors_get_all_subfeatures(chip, feat, &k))) {
                    if(subfeat->type != SENSORS_SUBFEATURE_TEMP_INPUT) continue;
                    sensors_get_value(chip, subfeat->number, &value);

                    /* Count sensors above the low threshold. */
                    if(value >= low) nlow++;
                    /* Count sensors above the critical threshold. */
                    if(value >= critical) ncrit++;
                    /* Compute the average over all sensors. */
                    sum += value;
                    n++;
                }
            }            
        }

        /* Increase the number of triggering measures based on the trigger 
         * method. */
        switch(trigger) {
            case ANY_SENSOR:
                /* Increase tlow/tcrit if one is above the threshold. */
                if(nlow > 0) tlow++;
                if(ncrit > 0) tcrit++;
                break;
            case ALL_SENSORS:
                /* Increase tlow/tcrit if all are above the threshold. */
                if(nlow >= n) tlow++;
                if(ncrit >= n) tcrit++;
                break;
            case MEAN_VALUE:
                /* Increase tlow/tcrit if the average is over the threshold. */
                if(sum / n >= low) tlow++;
                if(sum / n >= critical) tcrit++;
                break;
        }

        if(tlow > takein || tcrit > takein) {
            /* We've hit a threshold too many times: notify. */
            icon = tcrit > takein ? "dialog-warning" : "dialog-information";
            urgency = tcrit > takein ?
                NOTIFY_URGENCY_CRITICAL :
                NOTIFY_URGENCY_NORMAL;

            notif = notify_notification_new("Temperature alert", "Careful, "
                                            "your system is reaching high "
                                            "temperatures.", icon);
            notify_notification_set_urgency(notif, urgency);
            notify_notification_show(notif, NULL);
            g_object_unref(G_OBJECT(notif));

            /* Stay silent for a bit. */
            if(tlow > takein) tlow = 0;
            if(tcrit > takein) tcrit = 0;
            sleep(wait);
        }
        else sleep(inter);
    }
    
    notify_uninit();
    sensors_cleanup();
    return EXIT_SUCCESS;
}

void sighandler(int signum) {
    /* Exit the mainloop upon signal reception. */
    running = 0;
}

void usage(char* prog) {
    fprintf(stderr, "%s [options]\n\n"
            "The notification triggers when:\n"
            "  -o   : at least one sensor reaches a threshold.\n"
            "  -a   : all sensors reach a threshold.\n"
            "  -m   : the mean temperature value reaches a threshold.\n\n"

            "Other options:\n"
            "  -l T : low threshold value (normal notification).\n"
            "  -c T : high threshold value (critical notification).\n"
            "  -i T : measure once every T seconds.\n"
            "  -n N : number of measures allowed above the threshold.\n"
            "  -w T : wait between two identical notifications.\n"
            "  -h   : print this text.\n", prog);
}
