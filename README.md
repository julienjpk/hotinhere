# hotinhere

`hotinhere` is a small C program meant to be started along with an X session. It
uses [`libsensors`][1] and [`libnotify`][2] to monitor system temperature and
sends a desktop notification when threshold values are hit too many times.

## Installing

You can build `hotinhere` using CMake:

    $ mkdir build; cd build
    $ cmake ../
    $ make
    $ sudo make install

## Using

Use command-line options to customise `hotinhere`'s behaviour. For more
information, check:

    $ hotinhere -h
    hotinhere [options]
    
    The notification triggers when:
      -o   : at least one sensor reaches a threshold.
      -a   : all sensors reach a threshold.
      -m   : the mean temperature value reaches a threshold.
    
    Other options:
      -l T : low threshold value (normal notification).
      -c T : high threshold value (critical notification).
      -i T : measure once every T seconds.
      -n N : number of measures allowed above the threshold.
      -w T : wait between two identical notifications.
      -h   : print this text.

By default:

- The notification triggers when a single sensor breaks the threshold.
- The thresholds are provided by the sensors. Fallback values of 80°C and 100°C
are used when this fails.
- Measures are taken every 5 seconds.
- There's a 10 minutes wait after a notification.

Typically, one would have `hotinhere` start with their X sessions, adding the
following line (probably with options) to their `~/.xinitrc` file:

    hotinhere &

[1]: https://linux.die.net/man/3/libsensors
[2]: https://developer.gnome.org/libnotify/
